// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  token : 'eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJzd2FteSIsImlhdCI6MTU4Mjg3ODk1NCwic3ViIjoiSldUIFRva2VuIiwiaXNzIjoiSmF2YVRwb2ludCIsImV4cCI6MTU4MjkyMjE1NH0.2TvUQ4AYC6oZYPbimmESFznBh9413zJu5VheFFhUkkA',
  baseUrl: 'http://localhost:8080/authApi/',
  migrationUrl: 'http://localhost:8080/migration/',
  imageUrl: 'assets/retelzy-logo.png',
  rimDocs:'assets/RIM.pdf'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
