import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisainfoComponent } from './visainfo.component';

describe('VisainfoComponent', () => {
  let component: VisainfoComponent;
  let fixture: ComponentFixture<VisainfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisainfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisainfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
