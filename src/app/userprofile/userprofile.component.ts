import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  isEmpInfo:boolean = true;
  isAddInfo:boolean = false;
  isOrgInfo:boolean = false;
  isVisaInfo:boolean = false;
  isDocInfo:boolean = false;
  dtls: string = 'emp';
  empInfoStatus:boolean = true;
  addInfoStatus:boolean = false;
  orgInfoStatus:boolean = false;
  docInfoStatus:boolean = false;
  visaInfoStatus:boolean = false;

  empStatus:boolean = true;
  visaStatus:boolean = false;
  addStatus:boolean = false;
  
  isEmpSec:boolean = true;
  isVisaSec:boolean = false;
  enableinfo(){
    this.isEmpInfo = false;
  }

  empInfo(){
    this.isEmpInfo = true;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isVisaInfo = false;
    this.isDocInfo = false;

    this.empInfoStatus = true;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;

    this.visaStatus = false;
    this.empStatus = true;

    this.isVisaSec = false;
    this.isEmpSec = true;
      }

  addInfo(){
    this.isEmpInfo = false;
    this.isAddInfo = true;
    this.isOrgInfo = false;
    this.isVisaInfo = false;
    this.isDocInfo = false;
    this.empInfoStatus = false;
    this.addInfoStatus = true;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;
      }
  orgInfo(){
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = true;
    this.isVisaInfo = false;
    this.isDocInfo = false;
    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = true;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;
  }
  docInfo(){
    this.isVisaInfo = false;
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isDocInfo = true;
    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = true;
    this.visaInfoStatus = false;
  }
  visaInfo(){
    this.isVisaInfo = true;
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isDocInfo = false;

    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = true;

    this.visaStatus = true;
    this.empStatus = false;

    this.isVisaSec = true;
    this.isEmpSec = false;
  }

  logout(){
    this.router.navigate(["/login"]);
  }

}
