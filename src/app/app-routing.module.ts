import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressComponent } from './address/address.component';
import { DocumentComponent } from './document/document.component';
import { EmployeeComponent } from './employee/employee.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { OrganizationComponent } from './organization/organization.component';
import { ReportComponent } from './report/report.component';
import { ReviewComponent } from './review/review.component';
import { RoleprofileComponent } from './roleprofile/roleprofile.component';
import { UserinfoComponent } from './userinfo/userinfo.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { VisainfoComponent } from './visainfo/visainfo.component';

const routes: Routes = [{
  path: 'login',
  component: LoginComponent
},
{
  path: 'userinfo',
  component: UserinfoComponent
},
{
  path: 'employee',
  component: EmployeeComponent
},
{
  path: 'address',
  component: AddressComponent
},
{
  path: 'header',
  component: HeaderComponent
},
{
  path: 'organization',
  component: OrganizationComponent
},
{
  path: 'visainfo',
  component: VisainfoComponent
},
{
  path: 'document',
  component: DocumentComponent
},
{
  path: 'userprofile',
  component: UserprofileComponent
},
{
  path: 'roleprofile',
  component: RoleprofileComponent
},
{
  path: 'review',
  component: ReviewComponent
},
{
  path: 'report',
  component: ReportComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
