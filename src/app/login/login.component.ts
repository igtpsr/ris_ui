import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from "../services/admin.service";
import { AdminDetail } from "../classes/admin-detail";
import { IdleService } from "./../services/idle.service";
import { StorageService } from "../secret/storage.service";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private adminDetail = new AdminDetail();
  constructor(private router: Router,
    private adminService: AdminService,
    private idleService: IdleService,
    private storageService: StorageService,
    private toastr?: ToastrService
    ) { }
    status: boolean = false;
  ngOnInit(): void {
    if (this.adminService.isLoggedIn()) {
      this.router.navigate(["/userinfo"]);
    } else {
      this.router.navigate(["/login"]);
    }
  }
  form = new FormGroup({
    uName: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required),
  });
  login(){
    //this.router.navigate(["/userinfo"]);
  }

  Login(LoginInformation: any) {
    console.log("In Login Function"+this.uName?.value);
    //this.router.navigate(["/userinfo"]);
    this.adminDetail.userName = this.uName?.value;
    this.adminDetail.password = this.Password?.value;
    this.status = true;
    this.adminService.login(this.adminDetail).subscribe(
      (response) => {
        this.idleService.checkUserIdleOrNot();
        let result = response.body.token;
        this.status = false;
        if (result.length) {
          this.storageService.secureStorage.setItem("token", result);
          this.storageService.secureStorage.setItem("userName",  response.body.userName);
          this.storageService.secureStorage.setItem("roleId",  response.body.roleProfile.roleId);
          this.router.navigate(["/dashboard"]);
        } else {
          alert(
            "please register before login Or Invalid combination of Email and password"
          );
        }
      },
      (error) => {
        this.status = false;
        if (error.status == 403) {
          //this.toastr.error("Invalid Credentials");
        } else {
          //this.toastr.error("Server is not available");
        }
      }
    );
  }
  get uName() {
    return this.form.get("uName");
  }

  get Password() {
    return this.form.get("password");
  }
}
