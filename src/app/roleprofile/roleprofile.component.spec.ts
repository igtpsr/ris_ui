import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleprofileComponent } from './roleprofile.component';

describe('RoleprofileComponent', () => {
  let component: RoleprofileComponent;
  let fixture: ComponentFixture<RoleprofileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleprofileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
