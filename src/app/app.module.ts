import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UserinfoComponent } from './userinfo/userinfo.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { EmployeeComponent } from './employee/employee.component';
import { AddressComponent } from './address/address.component';
import { OrganizationComponent } from './organization/organization.component';
import { VisainfoComponent } from './visainfo/visainfo.component';
import { DocumentComponent } from './document/document.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { RoleprofileComponent } from './roleprofile/roleprofile.component';
import { ReviewComponent } from './review/review.component';
import { ReportComponent } from './report/report.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NgIdleKeepaliveModule } from "@ng-idle/keepalive";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserinfoComponent,
    HeaderComponent,
    FooterComponent,
    EmployeeComponent,
    AddressComponent,
    OrganizationComponent,
    VisainfoComponent,
    DocumentComponent,
    UserprofileComponent,
    RoleprofileComponent,
    ReviewComponent,
    ReportComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 2000,
    }),
    ToastContainerModule,
    NgIdleKeepaliveModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
