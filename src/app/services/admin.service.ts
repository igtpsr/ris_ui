import { Injectable } from '@angular/core';
import { StorageService } from "../secret/storage.service";
import { AdminDetail } from "../classes/admin-detail";
import { Observable, BehaviorSubject, from } from "rxjs";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";

interface status {
  Status: string;
}

interface logoutStatus {
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = environment.baseUrl;

  constructor(private storageService: StorageService,
    private http: HttpClient,
    private toastr: ToastrService,
    private router: Router) { }
  isLoggedIn(): boolean {
   // return !!this.storageService.secureStorage.getItem("token");
  return false;
  }

  login(adminDetail: AdminDetail): Observable<any> {
    let url = this.baseUrl + "brimLogin.do";
    return this.http.post(url, adminDetail, { observe: "response" as "body" })
  }

  logout() {
    let url = this.baseUrl + "logout.do";
    
    let token = this.storageService.secureStorage.getItem("token");
    let obj = { token: token };
    return this.http
      .put<logoutStatus>(url, obj)
      .subscribe(
        (res) => {
          if (res.message.toUpperCase() == "SUCCESS") {
            this.storageService.secureStorage.clear();
            this.router.navigate(["/login"]);
          } else {
            this.toastr.error("Something Went Wrong, Please contact to admin");
          }
        },
        (error) => {
          this.storageService.secureStorage.clear();
          this.router.navigate(["/login"]);
        }
      );
  }

  updateTheExpiryTimeOfToken() {
    var token = this.storageService.secureStorage.getItem("token");
    return this.http.put(this.baseUrl + "updateTheExpiryTimeOfToken", {
      token: token,
    });
  }
}
