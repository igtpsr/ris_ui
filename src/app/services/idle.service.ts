import { Injectable } from '@angular/core';
import { StorageService } from "./../secret/storage.service";
import { AdminService } from "./admin.service";
import { Router } from "@angular/router";
import { Idle, DEFAULT_INTERRUPTSOURCES } from "@ng-idle/core";
import { Keepalive } from "@ng-idle/keepalive";

@Injectable({
  providedIn: 'root'
})
export class IdleService {
  idleState = "Not started.";
  timedOut = false;
  lastPing?: Date;
  constructor(   private router: Router,
    private authService: AdminService,
    private idle: Idle,
    private keepalive: Keepalive,
    private storageService: StorageService) { }
  checkUserIdleOrNot() {
    console.log("Ng Idle Started for Watching...");

    // sets an idle timeout of 840 seconds.
    this.idle.setIdle(840);

    // sets a timeout period of 5 seconds. after 30 seconds of inactivity, the user will be considered timed out.
    this.idle.setTimeout(30);

    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    /*This function execute when user gone to idle state */
    this.idle.onIdleStart.subscribe(() => {
      this.idleState = "You've gone idle!";
      let isMigrationRunning = this.storageService.secureStorage.getItem(
        "migartionStatus"
      );
      console.log("Staus :"+ isMigrationRunning);
      console.log(this.idleState);
      //alert('You have gone Idle');
    });

    /*This function execute when we are going to show warning of logout after user went to idle state */
    this.idle.onTimeoutWarning.subscribe((countdown: string) => {
      this.idleState = "You will time out in " + countdown + " seconds!";
      console.log(this.idleState);
    });

    /*This function execute when our warning time over, and we will do time out or log out the page */
    this.idle.onTimeout.subscribe(() => {
      let isMigrationRunning = this.storageService.secureStorage.getItem(
        "migartionStatus"
      );

      if (isMigrationRunning) {
        this.timedOut = false;
        this.idle.watch();
      } else {
        this.idleState = "Timed out!";
        this.timedOut = true;
        console.log(this.idleState);
        location.reload();
        this.authService.logout();

      }
    });

    /* this function will execute when user ineterupt while showing timeout warning */
    this.idle.onIdleEnd.subscribe(() => {
      this.idleState = "No longer idle.";
      // console.log(this.idleState);
      this.reset();
    });

    // sets the ping interval to 15 seconds
    this.keepalive.interval(15);

    this.keepalive.onPing.subscribe(() => (this.lastPing = new Date()));

    this.idle.watch();
  }

  /*This function update the details when user will inetrrupt between the timeout warning */
  reset() {
    this.idle.watch();
    this.idleState = "Started.";
    this.timedOut = false;
    this.updateTheExpiryTimeOfToken();
  }

  private updateTheExpiryTimeOfToken() {
    this.authService.updateTheExpiryTimeOfToken().subscribe(
      (resp) => {
        console.log("Status :", resp);
      },
      (error) => {
        console.log(error);
        this.router.navigateByUrl("");
      }
    );
  }

  stopUsingNgIdle() {
    //console.log("Ng Idle Stopped For Watching...");
    this.idleState = "Timed out!";
    this.idle.stop();
    this.timedOut = true;
  }
}
