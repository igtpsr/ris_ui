import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userinfo',
  templateUrl: './userinfo.component.html',
  styleUrls: ['./userinfo.component.css']
})
export class UserinfoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  // to enable tab color
  empStatus:boolean = true;
  visaStatus:boolean = false;
  addStatus:boolean = false;
  reviewStatus:boolean = false;
  reportStatus:boolean = false;
  
  // To enable only sub section
  isEmpSec:boolean = true;
  isVisaSec:boolean = false;

  // To enable each and every tab information 
  empInfoStatus:boolean = true;
  addInfoStatus:boolean = false;
  orgInfoStatus:boolean = false;
  docInfoStatus:boolean = false;
  visaInfoStatus:boolean = false;

  // To enable component Selector
  isEmpInfo:boolean = true;
  isAddInfo:boolean = false;
  isOrgInfo:boolean = false;
  isVisaInfo:boolean = false;
  isDocInfo:boolean = false;
  isReviewInfo:boolean = false;
  isReport:boolean = false;

  empInfo(){

    this.visaStatus = false;
    this.empStatus = true;
    this.reviewStatus = false;
    this.reportStatus = false;

    this.isVisaSec = false;
    this.isEmpSec = true;

    this.empInfoStatus = true;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;

    this.isEmpInfo = true;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isVisaInfo = false;
    this.isDocInfo = false;
    this.isReviewInfo = false;
    this.isReport = false;

  }

  addInfo(){

    this.empInfoStatus = false;
    this.addInfoStatus = true;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;

    this.isEmpInfo = false;
    this.isAddInfo = true;
    this.isOrgInfo = false;
    this.isVisaInfo = false;
    this.isDocInfo = false;
    this.isReviewInfo = false;

  }
  orgInfo(){

    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = true;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;

    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = true;
    this.isVisaInfo = false;
    this.isDocInfo = false;
    this.isReviewInfo = false;

  }

  visaInfo(){

    this.visaStatus = true;
    this.empStatus = false;
    this.reviewStatus = false;
    this.reportStatus = false;

    this.isVisaSec = true;
    this.isEmpSec = false;

    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = true;

    this.isVisaInfo = true;
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isDocInfo = false;
    this.isReviewInfo = false;
    this.isReport = false;

  }
  docInfo(){

    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = true;
    this.visaInfoStatus = false;

    this.isVisaInfo = false;
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isDocInfo = true;
    this.isReviewInfo = false;
    this.isReport = false;

  }
  

  review(){

    this.reviewStatus = true;
    this.visaStatus = false;
    this.empStatus = false;
    this.reportStatus = false;

    this.isVisaSec = false;
    this.isEmpSec = false;

    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;

    this.isReviewInfo = true;
    this.isVisaInfo = false;
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isDocInfo = false;
    this.isReport = false;


  }
  report(){

    this.reviewStatus = false;
    this.visaStatus = false;
    this.empStatus = false;
    this.reportStatus = true;

    this.isVisaSec = false;
    this.isEmpSec = false;

    this.empInfoStatus = false;
    this.addInfoStatus = false;
    this.orgInfoStatus = false;
    this.docInfoStatus = false;
    this.visaInfoStatus = false;

    this.isReviewInfo = false;
    this.isVisaInfo = false;
    this.isEmpInfo = false;
    this.isAddInfo = false;
    this.isOrgInfo = false;
    this.isDocInfo = false;
    this.isReport = true;

  }

}
