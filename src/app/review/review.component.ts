import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  constructor() { }
  empStatus:boolean = true;
  isEditEmpInfo:boolean = false;
  isEditAddInfo:boolean = false;
  isEditOrgInfo:boolean = false;
  isEditVisaInfo:boolean = false;
  isEditDocInfo:boolean = false;

  ngOnInit(): void {
  }

  editInfo(){
    this.isEditEmpInfo = true;
    console.log(this.isEditEmpInfo);
  }

  saveEmp(){
    this.isEditEmpInfo = true;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = false;
  }
  saveAdd(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = true;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = false;
    console.log("Address"+this.isEditAddInfo);
  }
  saveOrg(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = true;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = false;
  }
  saveVisa(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = true;
    this.isEditDocInfo = false;
  }
  saveDoc(){
    this.isEditEmpInfo = false;
    this.isEditAddInfo = false;
    this.isEditOrgInfo = false;
    this.isEditVisaInfo = false;
    this.isEditDocInfo = true;
  }

}
